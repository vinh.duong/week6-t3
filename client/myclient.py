import http.client

target_hostname = "localhost"
target_port = 3000
default_timeout = 10

conn = http.client.HTTPConnection(
    host=target_hostname,
    port=target_port,
    timeout=default_timeout
)

headers = { 'user-agent': "vscode-restclient" }

# GET request to the server
conn.request("GET", "/", headers=headers)

res = conn.getresponse()
data = res.read()

print(data.decode("utf-8"))

# Query data
conn.request("GET", "/cryptic-data?cryptic_data=Grfgvat", headers=headers)

res = conn.getresponse()
data = res.read()

print(data.decode("utf-8"))
