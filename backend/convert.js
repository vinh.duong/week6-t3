import { createRequire } from 'module'

const require = createRequire(import.meta.url);
const GRAVITY = 9.81;
const readline = require("readline");

export class ROT13 {
    static encode(val, n = 13) {
        var splitgroup_l = CHARSET_LOWER().split(/[n]/, 1);
        var splitgroup_u = CHARSET_UPPER().split(/[N]/, 1);
        var alpha = CHARSET_LOWER() + splitgroup_l.toString() + CHARSET_UPPER() + splitgroup_u.toString();

        return val.replace(/[a-z]/gi, letter => alpha[alpha.indexOf(letter) + n]);
    };

    static decode(val, n = 13) {
        var splitgroup_l = CHARSET_LOWER().split(/[n]/, 1);
        var splitgroup_u = CHARSET_UPPER().split(/[N]/, 1);
        var alpha = CHARSET_LOWER() + splitgroup_l.toString() + CHARSET_UPPER() + splitgroup_u.toString();

        return val.replace(/[a-z]/gi, letter => alpha[alpha.indexOf(letter) + n]);
    };
};

export const CHARSET_LOWER = () => {
    var str = "";
    let startChar = "a";
    let endChar = "z";
    
    for (let c = startChar.charCodeAt(0); c <= endChar.charCodeAt(0); c++) {
        str += String.fromCharCode(c);
    };
    return str;
};

export const CHARSET_UPPER = () => {
    var str = "";
    let startChar = "A";
    let endChar = "Z";
    
    for (let c = startChar.charCodeAt(0); c <= endChar.charCodeAt(0); c++) {
        str += String.fromCharCode(c);
    };
    return str;
};