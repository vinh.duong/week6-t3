import { ROT13 } from './convert.js';
import { createRequire } from 'module'
const require = createRequire(import.meta.url);

const express = require('express');
const app = express();
const port = 3000;

/**
 * This is our first endpoint "/"
 */

app.get('/', (req, res) => {
    res.send('Got your message');
});

/**
 * This is endpoint "/data"
 */

 app.get('/cryptic-data', (req, res) => {
    const cryptic_data = req.query.cryptic_data;
    console.log(`Receiving: ${cryptic_data}`);
    const mess = ROT13.decode(cryptic_data);
    console.log("Decode: " + mess);
    res.send("Decode: " + mess);
});

app.listen(port, () => console.log(`Listening on ${port}`));